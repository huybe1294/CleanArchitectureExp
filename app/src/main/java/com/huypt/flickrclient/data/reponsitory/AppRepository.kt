package com.huypt.flickrclient.data.reponsitory

import android.util.Log
import com.huypt.flickrclient.data.entity.FlickrResentEntity
import com.huypt.flickrclient.data.entity.mapper.EntityJsonMapper
import com.huypt.flickrclient.domain.model.FlickrResent
import com.huypt.flickrclient.domain.repository.IAppRepository
import com.huypt.flickrclient.util.CommonUtils
import java.io.BufferedInputStream
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class AppRepository(private var src: String) : IAppRepository {

    override fun getListPhoto(): FlickrResent? {
        var flickrResentEntity: FlickrResentEntity? = null
        var conn: HttpURLConnection? = null
        try {
            val url = URL(src)
            val data: String
            conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = "GET"
            conn.setRequestProperty("Content-Type", "application/json")

            if (conn.responseCode == HttpURLConnection.HTTP_OK) {
                val stream = BufferedInputStream(conn.inputStream)
                data = CommonUtils.readStream(inputStream = stream)
                flickrResentEntity = EntityJsonMapper.transformPhoto(data)
            } else {
                Log.e("responseCode: ", "${conn.responseCode}")
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            conn?.disconnect()
        }
        return flickrResentEntity?.toFlickrResent()
    }
}