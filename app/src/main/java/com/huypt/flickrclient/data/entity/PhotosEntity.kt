package com.huypt.flickrclient.data.entity

import com.google.gson.annotations.SerializedName
import com.huypt.flickrclient.domain.model.Photos

data class PhotosEntity(@SerializedName("perpage")
                        val perpage: Int = 0,
                        @SerializedName("total")
                        val total: Int = 0,
                        @SerializedName("pages")
                        val pages: Int = 0,
                        @SerializedName("photo")
                        val photoItem: List<PhotoItemEntity>,
                        @SerializedName("page")
                        val page: Int = 0) {


    fun toPhotos() = Photos(perpage,
            total,
            pages,
            photoItem.map { it -> it.toPhotoItem() },
            page)
}