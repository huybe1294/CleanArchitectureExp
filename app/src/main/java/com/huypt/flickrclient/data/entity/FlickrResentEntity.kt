package com.huypt.flickrclient.data.entity

import com.google.gson.annotations.SerializedName
import com.huypt.flickrclient.domain.model.FlickrResent

data class FlickrResentEntity(@SerializedName("stat")
                              val stat: String = "",
                              @SerializedName("photos")
                              val photos: PhotosEntity) {


    fun toFlickrResent() = FlickrResent(stat, photos.toPhotos())
}