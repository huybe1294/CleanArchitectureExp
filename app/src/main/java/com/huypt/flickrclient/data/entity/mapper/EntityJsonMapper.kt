package com.huypt.flickrclient.data.entity.mapper

import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import com.huypt.flickrclient.FlickrApplication
import com.huypt.flickrclient.data.entity.FlickrResentEntity

object EntityJsonMapper {

    @Throws(JsonSyntaxException::class)
    fun transformPhoto(photoResponse: String): FlickrResentEntity {
        val flickrEntityType = object : TypeToken<FlickrResentEntity>() {}.type
        return FlickrApplication.instance.gson.fromJson(photoResponse, flickrEntityType)
    }
}