package com.huypt.flickrclient.data.entity

import com.google.gson.annotations.SerializedName
import com.huypt.flickrclient.domain.model.PhotoItem

data class PhotoItemEntity(@SerializedName("owner")
                           val owner: String = "",
                           @SerializedName("server")
                           val server: String = "",
                           @SerializedName("height_s")
                           val heightS: String = "",
                           @SerializedName("width_s")
                           val widthS: String = "",
                           @SerializedName("url_s")
                           val urlS: String = "",
                           @SerializedName("ispublic")
                           val ispublic: Int = 0,
                           @SerializedName("isfriend")
                           val isfriend: Int = 0,
                           @SerializedName("farm")
                           val farm: Int = 0,
                           @SerializedName("id")
                           val id: String = "",
                           @SerializedName("secret")
                           val secret: String = "",
                           @SerializedName("title")
                           val title: String = "",
                           @SerializedName("isfamily")
                           val isfamily: Int = 0) {


    fun toPhotoItem() = PhotoItem(owner, server, heightS, widthS, urlS, ispublic, isfriend,
            farm, id, secret, title, isfamily)
}