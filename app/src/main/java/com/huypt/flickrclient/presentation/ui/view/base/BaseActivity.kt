package com.huypt.flickrclient.presentation.ui.view.base

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.huypt.flickrclient.util.FragmentUtil
import org.jetbrains.anko.AnkoLogger

abstract class BaseActivity : AppCompatActivity(), AnkoLogger, View.OnClickListener {

    @IdRes
    open fun getContainerId(): Int = 0

    /*====================[START] FRAGMENT CONTROLLER ==========================*/
    private var fragmentController: FragmentController? = null

    @LayoutRes
    protected abstract fun bindLayout(): Int

    protected abstract fun initData(bundle: Bundle?)

    protected abstract fun initView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle = intent.extras
        initData(bundle)

        setContentView(bindLayout())
        initFragmentController()
        initView()
    }


    override fun onBackPressed() {
        val fragment = FragmentUtil.getCurrentFragment(this)
        if (fragment is BaseFragment && fragment.onBackPressed()) {
            // TODO: 9/7/2018
        } else {
            super.onBackPressed()
        }
    }

    private fun initFragmentController() {
        if (getContainerId() != 0) {
            fragmentController = FragmentController(supportFragmentManager, getContainerId())
        } else {
        }
    }

    @Throws(NullPointerException::class)
    fun switchScreenOnContainer(baseFragment: BaseFragment, option: FragmentController.Option) {
        val fragmentController = fragmentController
        fragmentController?.switchFragmentWithInstance(baseFragment, option)
                ?: throw NullPointerException("Fragment controller null")
    }


    @Throws(NullPointerException::class)
    fun switchScreenOnContainer(baseFragment: BaseFragment?) {
        if (baseFragment != null) {
            val option = FragmentController.Option.Builder()
                    .setTag(baseFragment.javaClass.simpleName)
                    .option
            switchScreenOnContainer(baseFragment, option)
        } else {
            throw NullPointerException("Instance fragment is null")
        }
    }
    /*========================[END] FRAGMENT CONTROLLER ==========================================*/

}
