package com.huypt.flickrclient.presentation.presenter

interface BaseView {
    fun showProgress();

    fun hideProgress();

    fun showMessage(message: String);
}