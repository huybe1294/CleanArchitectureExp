package com.huypt.flickrclient.presentation.presenter.search

import android.location.Location
import com.huypt.flickrclient.data.entity.FlickrResentEntity
import com.huypt.flickrclient.presentation.model.FlickrResentModel
import com.huypt.flickrclient.presentation.presenter.BaseView

interface SearchContract {
    interface Presenter {
        fun getPhotoList(src:String)
    }

    interface View : BaseView {
        fun displayPhotoList(flickrResent: FlickrResentModel)
    }
}