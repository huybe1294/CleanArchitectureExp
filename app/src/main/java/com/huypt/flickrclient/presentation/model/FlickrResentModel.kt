package com.huypt.flickrclient.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FlickrResentModel(
        val stat: String = "",
        val photos: PhotosModel) : Parcelable