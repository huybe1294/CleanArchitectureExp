package com.huypt.flickrclient.presentation.presenter.splash

import com.huypt.flickrclient.presentation.model.FlickrResentModel
import com.huypt.flickrclient.presentation.presenter.BaseView

interface SplashContract {
    interface Presenter {
        fun getPhotoList(src:String)
    }

    interface View : BaseView {
        fun getPhotoListSuccess(flickrResent: FlickrResentModel)
    }
}