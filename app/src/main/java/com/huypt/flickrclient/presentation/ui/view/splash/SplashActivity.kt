package com.huypt.flickrclient.presentation.ui.view.splash

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.huypt.flickrclient.R
import com.huypt.flickrclient.util.constant.AppConstants
import com.huypt.flickrclient.presentation.ui.view.base.BaseActivity
import com.huypt.flickrclient.presentation.model.FlickrResentModel
import com.huypt.flickrclient.presentation.presenter.splash.SplashContract
import com.huypt.flickrclient.presentation.presenter.splash.SplashPresenter
import com.huypt.flickrclient.presentation.ui.view.main.MainActivity
import com.huypt.flickrclient.util.CommonUtils

class SplashActivity : BaseActivity(), SplashContract.View {
    private lateinit var mPresenter: SplashContract.Presenter

    override fun onClick(p0: View?) {
    }

    override fun bindLayout() = R.layout.activity_splash

    override fun initData(bundle: Bundle?) {
        mPresenter = SplashPresenter(this)
        mPresenter.getPhotoList(AppConstants.API_GET_RESENT)
    }

    override fun initView() {
    }

    override fun getPhotoListSuccess(flickrResent: FlickrResentModel) {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra(AppConstants.KEY_FLICKR_RESENT, flickrResent)
        startActivity(intent)
        finish()
    }

    override fun showProgress() {
        //do nothing
    }

    override fun hideProgress() {
        //do nothing
    }

    override fun showMessage(message: String) {
        CommonUtils.toastMessage(this, message)
    }
}