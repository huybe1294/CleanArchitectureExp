package com.huypt.flickrclient.presentation.presenter.search

import com.huypt.flickrclient.AppExecutors
import com.huypt.flickrclient.data.reponsitory.AppRepository
import com.huypt.flickrclient.domain.executor.IExecutor
import com.huypt.flickrclient.domain.interactors.BaseInteractor
import com.huypt.flickrclient.domain.interactors.IPhotoInteractor
import com.huypt.flickrclient.domain.interactors.impl.PhotoInteractor
import com.huypt.flickrclient.domain.model.FlickrResent
import com.huypt.flickrclient.domain.repository.IAppRepository

class SearchPresenter(private val mView: SearchContract.View) : SearchContract.Presenter, IPhotoInteractor.CallBack {

    override fun getPhotoList(src: String) {
        mView.showProgress()

        val repository: IAppRepository = AppRepository(src)
        val executors: IExecutor = AppExecutors.instance

        val interactor: BaseInteractor = PhotoInteractor(this, executors, repository)
        interactor.execute()
    }

    override fun onMessageRetrieved(flickrResent: FlickrResent) {
        mView.hideProgress()
        mView.displayPhotoList(flickrResent.toFlickrResentModel())
    }

    override fun onRetrievalFailed(error: String) {
        mView.hideProgress()
        mView.showMessage(error)
    }
}