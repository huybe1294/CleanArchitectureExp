package com.huypt.flickrclient.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotoItemModel(
        val owner: String = "",
        val server: String = "",
        val heightS: String = "",
        val widthS: String = "",
        val urlS: String = "",
        val ispublic: Int = 0,
        val isfriend: Int = 0,
        val farm: Int = 0,
        val id: String = "",
        val secret: String = "",
        val title: String = "",
        val isfamily: Int = 0) : Parcelable {

}