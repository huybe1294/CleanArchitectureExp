package com.huypt.flickrclient.presentation.ui.view.base

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import org.jetbrains.anko.AnkoLogger

abstract class BaseFragment : Fragment(), AnkoLogger , View.OnClickListener{

    private var mContentView: View? = null

    private val isActivityAvailable: Boolean
        get() = activity != null && !activity!!.isFinishing

    protected val baseActivity: BaseActivity?
        get() {
            val activity = activity
            return if (isActivityAvailable && activity is BaseActivity) {
                activity
            } else {
                null
            }
        }

    /* ============== [START] FRAGMENT CONTROLLER =====================*/
    private var fragmentController: FragmentController? = null

    @IdRes
    open fun getContainerId(): Int = 0

    @LayoutRes
    protected abstract fun bindLayout(): Int

    protected abstract fun initData(bundle: Bundle?)

    protected abstract fun initView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initFragmentController()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (mContentView == null) {
            mContentView = inflater.inflate(bindLayout(), container, false)
        }
        return mContentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = arguments
        initData(bundle)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initView()
    }

    fun onBackPressed(): Boolean {
        return false
    }

    private fun initFragmentController() {
        if (getContainerId() != 0) {
            fragmentController = FragmentController(childFragmentManager, getContainerId())
        }
    }

    /**
     * This is method switch screen on container from [.getContainerId]
     *
     * @param baseFragment
     * @param option
     */
    @Throws(NullPointerException::class)
    fun switchScreenOnContainer(baseFragment: BaseFragment, option: FragmentController.Option) {
        if (isActivityAvailable) {
            val fragmentController = fragmentController
            fragmentController?.switchFragmentWithInstance(baseFragment, option)
                    ?: throw NullPointerException("Fragment controller null")
        } else {
        }
    }

    @Throws(NullPointerException::class)
    fun switchScreenOnContainer(baseFragment: BaseFragment?) {
        if (baseFragment != null) {
            val option = FragmentController.Option.Builder()
                    .setTag(baseFragment.javaClass.simpleName)
                    .option
            switchScreenOnContainer(baseFragment, option)
        } else {
            throw NullPointerException("Instance fragment is null")
        }
    }
    /*============== [END] FRAGMENT CONTROLLER =====================*/
}
