package com.huypt.flickrclient.presentation.ui.adapter

import android.content.Context
import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.huypt.flickrclient.R
import com.huypt.flickrclient.presentation.ui.view.base.BaseActivity
import com.huypt.flickrclient.presentation.ui.view.photo_detail.PhotoFragment
import org.jetbrains.anko.displayMetrics

class PhotoAdapter(private var context: Context, private var listPhotoUrl: List<String>) : RecyclerView.Adapter<PhotoAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_image, parent, false)

        val width = context.displayMetrics.widthPixels
        view.layoutParams.width = width / 3
        view.layoutParams.height = width / 3
        view.requestLayout()

        return ViewHolder(view)
    }

    override fun getItemCount() = listPhotoUrl.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemPhoto = listPhotoUrl[position]

        /*Load photoItem*/
        Glide.with(context).load(itemPhoto).apply(RequestOptions().centerCrop()).into(holder.imgFlickrResent)

        /*Onclick*/
        holder.imgFlickrResent.setOnClickListener {
            if (context is BaseActivity) {
                (context as BaseActivity).switchScreenOnContainer(PhotoFragment.newInstance(itemPhoto))
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var imgFlickrResent: ImageView = itemView.findViewById(R.id.image)
    }

    interface CallBack {
        fun onItemClick(bitmap: Bitmap)
    }
}