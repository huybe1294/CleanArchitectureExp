package com.huypt.flickrclient.presentation.ui.view.photo_detail

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.huypt.flickrclient.R
import com.huypt.flickrclient.presentation.ui.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_photo.*

class PhotoFragment : BaseFragment() {

    private lateinit var url: String

    companion object {
        fun newInstance(url: String): PhotoFragment {
            val fragment = PhotoFragment()
            fragment.url = url
            return fragment
        }
    }

    override fun bindLayout() = R.layout.fragment_photo

    override fun initData(bundle: Bundle?) {
    }

    override fun initView() {
        Glide.with(context!!).load(url).into(img_photo)
        btn_back.setOnClickListener(this)
    }


    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btn_back -> {
                baseActivity?.onBackPressed()
            }
        }
    }
}