package com.huypt.flickrclient.presentation.ui.view.main

import android.Manifest
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.huypt.flickrclient.FlickrApplication
import com.huypt.flickrclient.R
import com.huypt.flickrclient.presentation.ui.view.base.BaseActivity
import com.huypt.flickrclient.util.constant.AppConstants
import com.huypt.flickrclient.presentation.model.FlickrResentModel
import com.huypt.flickrclient.presentation.ui.adapter.PhotoAdapter
import com.huypt.flickrclient.presentation.ui.broadcast_receiver.AlarmReceiver
import com.huypt.flickrclient.util.GPSUtil
import com.huypt.flickrclient.presentation.ui.view.search.SearchLocationFragment
import com.huypt.flickrclient.presentation.ui.view.search.SearchFragment
import com.huypt.flickrclient.util.AppSharedPreference
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class MainActivity : BaseActivity(), GPSUtil.CallBack {

    private var rcvAdapter: PhotoAdapter? = null
    private lateinit var flickrResent: FlickrResentModel
    private var mListPhotoUrl: MutableList<String> = mutableListOf()

    override fun bindLayout() = R.layout.activity_main

    override fun getContainerId() = R.id.main_container

    override fun initData(bundle: Bundle?) {
        flickrResent = intent.getParcelableExtra(AppConstants.KEY_FLICKR_RESENT)
        for (item in flickrResent.photos.photoItem) {
            if (!item.urlS.isEmpty()) {
                mListPhotoUrl.add(item.urlS)
            }
        }
    }

    override fun initView() {
        img_search.setOnClickListener(this)
        img_location.setOnClickListener(this)
        img_notification.setOnClickListener(this)

        img_notification.isSelected = AppSharedPreference.getInstance()[AppConstants.PREFS_NOTIFI, Boolean::class.java]

        rcvAdapter = PhotoAdapter(this, mListPhotoUrl)
        rcv_image.layoutManager = GridLayoutManager(this, 3)
        rcv_image.adapter = rcvAdapter
    }

    /*=== ONCLICK ===*/
    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.img_search -> {
                switchScreenOnContainer(SearchFragment())
            }

            R.id.img_location -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        GPSUtil.requestPermissions(this)
                    } else {
                        if (GPSUtil.isOpenGPS(this)) {
                            GPSUtil.getLocation(this)
                        } else {
                            GPSUtil.requestGPS(this)
                        }
                    }
                } else {
                    // Permission has been granted -> GPS checking
                    if (GPSUtil.isOpenGPS(this)) {
                        GPSUtil.getLocation(this)
                    } else {
                        GPSUtil.requestGPS(this)
                    }
                }
            }

            R.id.img_notification -> {
                val alarmMgr = this.getSystemService(Context.ALARM_SERVICE) as AlarmManager
                val alarmIntent = Intent(this, AlarmReceiver::class.java).let { intent ->
                    PendingIntent.getBroadcast(FlickrApplication.instance, 0, intent, 0)
                }
                if (!img_notification.isSelected) {
                    img_notification.isSelected = true
                    alarmMgr.setRepeating(
                            AlarmManager.ELAPSED_REALTIME_WAKEUP,
                            SystemClock.elapsedRealtime(),
                            60 * 1000,
                            alarmIntent
                    )
                    AppSharedPreference.getInstance().put(AppConstants.PREFS_NOTIFI, true)
                    Snackbar.make(layout_parent, "Put notification is on", Snackbar.LENGTH_SHORT).show()
                } else {
                    img_notification.isSelected = false
                    alarmMgr.cancel(alarmIntent)
                    AppSharedPreference.getInstance().put(AppConstants.PREFS_NOTIFI, false)
                    Snackbar.make(layout_parent, "Put notification is off", Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    // Permission result
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode != AppConstants.REQUEST_CODE_PERMISSION) {
            return
        }
        if (GPSUtil.isPermissionGranted(permissions, grantResults, Manifest.permission.ACCESS_FINE_LOCATION)) run {
            if (GPSUtil.isOpenGPS(this)) {
                GPSUtil.getLocation(this)
            } else {
                GPSUtil.requestGPS(this)
            }
        } else {
            toast("Vui lòng cấp quyền sử dụng vị trí")
        }
    }

    // GPS  result
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != AppConstants.REQUEST_CODE_LOCATION) {
            return
        }
        if (resultCode == Activity.RESULT_OK) {
            GPSUtil.getLocation(this)
        } else {
            toast("Vui vui lòng bật GPS !")
        }
    }

    override fun onLocationReady(location: Location) {
        val lat = location.latitude
        val lon = location.longitude
        switchScreenOnContainer(SearchLocationFragment.newInstance(lat, lon))
    }
}
