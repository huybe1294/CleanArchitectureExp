package com.huypt.flickrclient.presentation.ui.view.search

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SearchView
import android.view.View
import com.huypt.flickrclient.R
import com.huypt.flickrclient.presentation.model.FlickrResentModel
import com.huypt.flickrclient.presentation.presenter.search.SearchContract
import com.huypt.flickrclient.presentation.presenter.search.SearchPresenter
import com.huypt.flickrclient.presentation.ui.view.base.BaseFragment
import com.huypt.flickrclient.presentation.ui.view.base.FragmentController
import com.huypt.flickrclient.presentation.ui.adapter.PhotoAdapter
import com.huypt.flickrclient.util.constant.AppConstants
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : BaseFragment(), SearchContract.View {
    private lateinit var mPresenter: SearchContract.Presenter
    private var rcvAdapter: PhotoAdapter? = null
    private var mListPhotoUrl: MutableList<String> = mutableListOf()
    private var tagFragment: String = ""

    companion object {
        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }

    override fun bindLayout() = R.layout.fragment_search

    override fun initData(bundle: Bundle?) {
        mPresenter = SearchPresenter(this)
    }

    override fun initView() {
        btn_back.setOnClickListener(this)
        view_transparent.setOnClickListener(this)
        view_transparent.visibility = View.GONE

        search_view_photo.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                view_transparent.visibility = View.VISIBLE
                if (query != null) {
                    if (!query.isEmpty()) {
                        mListPhotoUrl.clear()
                        mPresenter.getPhotoList(AppConstants.getApiSearch(query))
                        tagFragment = query
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btn_back -> {
                baseActivity?.onBackPressed()
            }

            R.id.view_transparent -> {
                val option = FragmentController.Option.Builder()
                        .setTag(BaseFragment::class.java.simpleName + tagFragment)
                        .isTransactionReplace(false)
                        .addBackStack(true)
                        .option
                baseActivity?.switchScreenOnContainer(SearchFragment.newInstance(), option)
            }
        }
    }

    override fun displayPhotoList(flickrResent: FlickrResentModel) {
        for (item in flickrResent.photos.photoItem) {
            if (!item.urlS.isEmpty()) {
                mListPhotoUrl.add(item.urlS)
            }
        }
        rcvAdapter = PhotoAdapter(context!!, mListPhotoUrl)
        rcv_image.layoutManager = GridLayoutManager(context, 3)
        rcv_image.adapter = rcvAdapter
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showMessage(message: String) {
    }
}