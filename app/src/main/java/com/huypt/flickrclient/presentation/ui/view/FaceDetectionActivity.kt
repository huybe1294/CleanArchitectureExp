package com.huypt.flickrclient.presentation.ui.view

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import com.google.firebase.FirebaseApp
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.huypt.flickrclient.R
import com.huypt.flickrclient.presentation.ui.view.base.BaseActivity
import com.huypt.flickrclient.util.CommonUtils
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceLandmark
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions


class FaceDetectionActivity : BaseActivity() {
    private val faces = mutableListOf<FirebaseVisionFace>()

    override fun bindLayout() = R.layout.activity_face_detection

    override fun initData(bundle: Bundle?) {
    }

    override fun initView() {
        val imageBitmap = BitmapFactory.decodeResource(this.resources, R.drawable.image)
        val image = FirebaseVisionImage.fromBitmap(imageBitmap)
        val options = FirebaseVisionFaceDetectorOptions.Builder()
                .setModeType(FirebaseVisionFaceDetectorOptions.ACCURATE_MODE)
                .setLandmarkType(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
                .setClassificationType(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                .setMinFaceSize(0.15f)
                .setTrackingEnabled(true)
                .build()
        val detector = FirebaseVision.getInstance().getVisionFaceDetector(options)
        detector.detectInImage(image)
                .addOnSuccessListener {
                    // Task completed successfully
                    // ...
                    CommonUtils.toastMessage(this, "Nhận diện thành công")
                    for (face in it) {
                        val bounds = face.boundingBox
                        val rotY = face.headEulerAngleY  // Head is rotated to the right rotY degrees
                        val rotZ = face.headEulerAngleZ  // Head is tilted sideways rotZ degrees

                        // If landmark detection was enabled (mouth, ears, eyes, cheeks, and
                        // nose available):

                        // Miệng trái
                        val leftMOUTH = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_MOUTH)
                        // Miệng phải
                        val rightMOUTH = face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_MOUTH)
                        // mắt trái
                        val leftEyes = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EYE)
                        // mắt phải
                        val rightEyes = face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_EYE)
                        // Tai trái
                        val leftEar = face.getLandmark(FirebaseVisionFaceLandmark.LEFT_EAR)
                        // Tai phải
                        val rightEar = face.getLandmark(FirebaseVisionFaceLandmark.RIGHT_EAR)
                        if (leftEar != null) {
                            val leftEarPos = leftEar.position
                        }

                        // If classification was enabled:
                        if (face.smilingProbability != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
                            val smileProb = face.smilingProbability
                        }
                        if (face.rightEyeOpenProbability != FirebaseVisionFace.UNCOMPUTED_PROBABILITY) {
                            val rightEyeOpenProb = face.rightEyeOpenProbability
                        }

                        // If face tracking was enabled:
                        if (face.trackingId != FirebaseVisionFace.INVALID_ID) {
                            val id = face.trackingId
                        }
                    }
                }
                .addOnFailureListener {
                    // Task failed with an exception
                    // ...
                    CommonUtils.toastMessage(this, "Nhận diện thất bại")
                }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.image -> {

            }
        }
    }
}