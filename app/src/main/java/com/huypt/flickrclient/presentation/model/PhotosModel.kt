package com.huypt.flickrclient.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class PhotosModel(
        val perpage: Int = 0,
        val total: Int = 0,
        val pages: Int = 0,
        val photoItem: List<PhotoItemModel>,
        val page: Int = 0) : Parcelable