package com.huypt.flickrclient.presentation.ui.view.search

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.huypt.flickrclient.R
import com.huypt.flickrclient.presentation.model.FlickrResentModel
import com.huypt.flickrclient.presentation.presenter.search.SearchContract
import com.huypt.flickrclient.presentation.presenter.search.SearchPresenter
import com.huypt.flickrclient.presentation.ui.adapter.PhotoAdapter
import com.huypt.flickrclient.presentation.ui.view.base.BaseFragment
import com.huypt.flickrclient.util.constant.AppConstants
import kotlinx.android.synthetic.main.fragment_search_location.*

class SearchLocationFragment : BaseFragment(), SearchContract.View {

    private lateinit var mPresenter: SearchContract.Presenter
    private var rcvAdapter: PhotoAdapter? = null
    private var mListPhotoUrl: MutableList<String> = mutableListOf()

    companion object {
        fun newInstance(lat: Double, lon: Double): SearchLocationFragment {
            val fragment = SearchLocationFragment()
            val args = Bundle()
            args.putDouble("lat", lat)
            args.putDouble("lon", lon)
            fragment.arguments = args
            return fragment
        }
    }

    override fun bindLayout() = R.layout.fragment_search_location

    override fun initData(bundle: Bundle?) {
        mPresenter = SearchPresenter(this)
        val lat = bundle?.getDouble("lat")
        val lon = bundle?.getDouble("lon")
        if (lat != null && lon != null)
            mPresenter.getPhotoList(AppConstants.getApiSearchLocation(lat, lon))
    }

    override fun initView() {
    }

    override fun onClick(p0: View?) {}

    override fun displayPhotoList(flickrResent: FlickrResentModel) {
        for (item in flickrResent.photos.photoItem) {
            if (!item.urlS.isEmpty()) {
                mListPhotoUrl.add(item.urlS)
            }
        }
        rcvAdapter = PhotoAdapter(context!!, mListPhotoUrl)
        rcv_image.layoutManager = GridLayoutManager(context, 3)
        rcv_image.adapter = rcvAdapter
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showMessage(message: String) {
    }
}