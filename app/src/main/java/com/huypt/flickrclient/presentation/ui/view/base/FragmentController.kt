package com.huypt.flickrclient.presentation.ui.view.base

import android.annotation.SuppressLint
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.huypt.flickrclient.R

class FragmentController(private val mFragmentManager: FragmentManager, @param:IdRes private val mLayoutContainerId: Int) {

    val currentFragment: Fragment
        get() = mFragmentManager.findFragmentById(mLayoutContainerId)

    @SuppressLint("CommitTransaction")
    fun switchFragmentWithInstance(fragmentInstance: Fragment?, option: Option): Int {
        if (this.isFragmentCurrentExists(option.tag)) {
            return EXISTS_FRAGMENT_IN_BACK_STACK
        }

        val isExistsBackStack = mFragmentManager.popBackStackImmediate(option.tag, 0);

        if (!isExistsBackStack) {
            if (fragmentInstance == null) {
                return UNKNOWN_INSTANCE_FRAGMENT
            }

            val transaction = mFragmentManager.beginTransaction()
            if (option.isUseAnimation) {
                transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
            }

            if (option.isTransactionReplace) {
                transaction.replace(mLayoutContainerId, fragmentInstance, option.tag)
            } else {
                transaction.add(mLayoutContainerId, fragmentInstance, option.tag)
            }

            if (option.isAddBackStack) {
                transaction.addToBackStack(option.tag)
            }
            return transaction.commit()
        }
        return EXISTS_FRAGMENT_IN_BACK_STACK
    }

    fun switchFragment(fragment: Class<out Fragment>, option: Option): Int {
        var fragmentInstance: Fragment? = null
        try {
            fragmentInstance = fragment.newInstance()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }

        return if (fragmentInstance == null) {
            UNKNOWN_INSTANCE_FRAGMENT
        } else switchFragmentWithInstance(fragmentInstance, option)
    }

    private fun isFragmentCurrentExists(tag: String?): Boolean {
        var ret = false
        val countFragmentInBackStack = mFragmentManager.backStackEntryCount

        if (countFragmentInBackStack > 0) {
            val currentEntry = mFragmentManager.getBackStackEntryAt(countFragmentInBackStack - 1)
            if (currentEntry != null && currentEntry.name != null && currentEntry.name == tag) {
                ret = true
            }
        }
        return ret
    }

    class Option private constructor() {
        var isUseAnimation = true
            private set
        var isAddBackStack = true
            private set
        var isTransactionReplace = true
            private set
        var tag: String? = null

        class Builder {
            val option: Option = Option()

            fun setTag(tag: String): Builder {
                this.option.tag = tag
                return this
            }

            fun useAnimation(use: Boolean): Builder {
                this.option.isUseAnimation = use
                return this
            }

            fun addBackStack(addBackStack: Boolean): Builder {
                this.option.isAddBackStack = addBackStack
                return this
            }

            fun isTransactionReplace(isTransactionReplace: Boolean): Builder {
                this.option.isTransactionReplace = isTransactionReplace
                return this
            }
        }
    }

    companion object {
        const val EXISTS_FRAGMENT_IN_BACK_STACK = -1
        const val UNKNOWN_INSTANCE_FRAGMENT = -1
    }

}
