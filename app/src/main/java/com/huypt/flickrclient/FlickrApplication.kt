package com.huypt.flickrclient

import android.app.Application
import com.google.firebase.FirebaseApp
import com.google.gson.Gson
import org.json.JSONObject

class FlickrApplication : Application() {
    lateinit var gson: Gson

    companion object {
        lateinit var instance: FlickrApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        FirebaseApp.initializeApp(this)
        gson = Gson()
    }
}