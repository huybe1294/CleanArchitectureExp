package com.huypt.flickrclient.util

import android.content.Context
import android.content.SharedPreferences
import com.huypt.flickrclient.FlickrApplication

class AppSharedPreference private constructor() {
    private val sharedPref: SharedPreferences

    companion object {
        private val PREFS_NAME = "share_prefs"

        @JvmStatic
        fun getInstance(): AppSharedPreference {
            return AppSharedPreference()
        }
    }

    init {
        sharedPref = FlickrApplication.instance.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    operator fun <T> get(key: String, anonymousClass: Class<T>): T {
        return when (anonymousClass) {
            String::class.java -> sharedPref.getString(key, "") as T
            Boolean::class.java -> java.lang.Boolean.valueOf(sharedPref.getBoolean(key, false)) as T
            Float::class.java -> java.lang.Float.valueOf(sharedPref.getFloat(key, 0f)) as T
            Int::class.java -> Integer.valueOf(sharedPref.getInt(key, 0)) as T
            Long::class.java -> java.lang.Long.valueOf(sharedPref.getLong(key, 0)) as T
            else -> FlickrApplication.instance.gson.fromJson(sharedPref.getString(key, ""), anonymousClass)
        }
    }

    operator fun <T> get(key: String, anonymousClass: Class<T>, defaultValue: T): T {
        return when (anonymousClass) {
            String::class.java -> sharedPref.getString(key, defaultValue as String) as T
            Boolean::class.java -> java.lang.Boolean.valueOf(sharedPref.getBoolean(key, defaultValue as Boolean)) as T
            Float::class.java -> java.lang.Float.valueOf(sharedPref.getFloat(key, defaultValue as Float)) as T
            Int::class.java -> Integer.valueOf(sharedPref.getInt(key, defaultValue as Int)) as T
            Long::class.java -> java.lang.Long.valueOf(sharedPref.getLong(key, defaultValue as Long)) as T
            else -> FlickrApplication.instance.gson.fromJson(sharedPref.getString(key, ""), anonymousClass)
        }
    }

    fun <T> put(key: String, data: T) {
        val editor = sharedPref.edit()
        when (data) {
            is String -> editor.putString(key, data as String)
            is Boolean -> editor.putBoolean(key, data as Boolean)
            is Float -> editor.putFloat(key, data as Float)
            is Int -> editor.putInt(key, data as Int)
            is Long -> editor.putLong(key, data as Long)
            else -> editor.putString(key, FlickrApplication.instance.gson.toJson(data))
        }
        editor.apply()
    }

    fun clear() {
        sharedPref.edit().clear().apply()
    }
}
