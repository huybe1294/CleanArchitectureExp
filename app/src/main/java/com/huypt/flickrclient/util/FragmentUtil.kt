package com.huypt.flickrclient.util

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager

object FragmentUtil {

    /**
     * find fragment
     *
     * @param context
     */
    fun getCurrentFragment(context: Context?): Fragment? {
        if (context == null) {
            return null
        }
        val fragmentManager = (context as FragmentActivity).supportFragmentManager

        var fragment: Fragment? = null
        val count = fragmentManager.backStackEntryCount
        if (count > 0) {
            fragment = fragmentManager.findFragmentByTag(fragmentManager.getBackStackEntryAt(count - 1).name)
        }
        return fragment
    }

    /**
     * clear all back stack
     *
     * @param context
     */
    fun clearAllBackStack(context: Context?) {
        if (context == null) {
            return
        }
        try {
            val fragmentManager = (context as FragmentActivity)
                    .supportFragmentManager
            if (fragmentManager.backStackEntryCount >= 0) {
                for (i in fragmentManager.backStackEntryCount - 1 downTo 0) {
                    removeFragmentNoAnimation(context, getCurrentFragment(context))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * remove fragment no animation
     *
     * @param context
     */
    private fun removeFragmentNoAnimation(context: Context?, fragment: Fragment?) {
        if (context == null) {
            return
        }
        try {
            val fragmentManager = (context as FragmentActivity)
                    .supportFragmentManager
            fragmentManager.popBackStack()
            fragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
