package com.huypt.flickrclient.util.constant

object AppConstants {
    const val REQUEST_CODE_PERMISSION = 101
    const val REQUEST_CODE_LOCATION = 102
    const val KEY_FLICKR_RESENT = "flickr_resent"
    const val PREFS_NOTIFI = "PREFS_NOTIFI"

    const val API_GET_RESENT = "https://api.flickr.com/services/rest/?api_key=73a8a1a69f168e13deaceb26a7889905&format=json&nojsoncallback=1&extras=url_s&method=flickr.photos.getRecent"
    private const val API_SEARCH = "https://api.flickr.com/services/rest/?api_key=73a8a1a69f168e13deaceb26a7889905&format=json&nojsoncallback=1&extras=url_s&method=flickr.photos.search&text="

    fun getApiSearch(str: String): String {
        return API_SEARCH + str
    }

    fun getApiSearchLocation(lat: Double, long: Double): String {
        return "https://api.flickr.com/services/rest/?api_key=73a8a1a69f168e13deaceb26a7889905&format=json&nojsoncallback=1&extras=url_s%2Cgeo&method=flickr.photos.search&lat=$lat&lon=$long"
    }

}