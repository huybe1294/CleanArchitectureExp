package com.huypt.flickrclient.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.support.v4.app.ActivityCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.huypt.flickrclient.util.constant.AppConstants

class GPSUtil(private var callBack: CallBack) {

    companion object {
        fun getLocation(context: Context) {
            val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
            try {
                fusedLocationClient.lastLocation
                        .addOnSuccessListener {
                            // Got last known location. In some rare situations this can be null.
                            // callBack.onLocationReady(it)
                        }
            } catch (e: SecurityException) {
                e.printStackTrace()
            }
        }

        fun requestGPS(context: Context) {
            // Create the location request and set the parameters.
            val locationRequest = LocationRequest().apply {
                interval = 10000
                fastestInterval = 5000
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }

            val builder = LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest)

            // Check whether the current location settings are satisfied.
            val task: Task<LocationSettingsResponse> = LocationServices.getSettingsClient(context)
                    .checkLocationSettings(builder.build())

            task.addOnFailureListener {
                if (it is ResolvableApiException) {
                    // Location settings are not satisfied.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        it.startResolutionForResult(context as Activity?,
                                AppConstants.REQUEST_CODE_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                }
            }
        }

        fun requestPermissions(context: Context) {
            ActivityCompat.requestPermissions(context as Activity,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                    AppConstants.REQUEST_CODE_PERMISSION)
        }

        fun isPermissionGranted(grantPermissions: Array<out String>, grantResults: IntArray,
                                permission: String): Boolean {
            for (i in grantPermissions.indices) {
                if (permission == grantPermissions[i]) {
                    return grantResults[i] == PackageManager.PERMISSION_GRANTED
                }
            }
            return false
        }

        fun isOpenGPS(context: Context): Boolean {
            val locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }
    }

    interface CallBack {
        fun onLocationReady(location: Location)
    }
}