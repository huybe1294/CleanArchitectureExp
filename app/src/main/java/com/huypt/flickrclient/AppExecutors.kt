package com.huypt.flickrclient

import android.os.Handler
import android.os.Looper
import com.huypt.flickrclient.domain.executor.IExecutor
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Global executor pools for the whole application.
 *
 * Grouping tasks like this avoids the effects of task starvation (e.g. disk reads don't wait behind
 * webservice requests).
 */
open class AppExecutors(
    private val diskIO: Executor,
    private val networkIO: Executor,
    private val mainThread: Executor
):IExecutor {

  companion object {
    val instance = AppExecutors()
  }

  private constructor() : this(
      Executors.newSingleThreadExecutor(),
      Executors.newFixedThreadPool(3),
      MainThreadExecutor()
  )

  override fun diskIO(): Executor {
    return diskIO
  }

  override fun networkIO(): Executor {
    return networkIO
  }

  override fun mainThread(): Executor {
    return mainThread
  }

  private class MainThreadExecutor : Executor {
    private val mainThreadHandler = Handler(Looper.getMainLooper())
    override fun execute(command: Runnable) {
      mainThreadHandler.post(command)
    }
  }
}
