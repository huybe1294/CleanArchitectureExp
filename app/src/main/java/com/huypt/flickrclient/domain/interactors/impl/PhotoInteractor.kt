package com.huypt.flickrclient.domain.interactors.impl

import com.huypt.flickrclient.domain.executor.IExecutor
import com.huypt.flickrclient.domain.interactors.IPhotoInteractor
import com.huypt.flickrclient.domain.model.FlickrResent
import com.huypt.flickrclient.domain.repository.IAppRepository

class PhotoInteractor(private val callBack: IPhotoInteractor.CallBack,
                      private val executors: IExecutor,
                      private val repository: IAppRepository) : IPhotoInteractor {

    override fun execute() {
        executors.networkIO().execute {
            executors.networkIO().execute {
                val flickrResent: FlickrResent? = repository.getListPhoto()

                if (flickrResent == null) {
                    executors.mainThread().execute {
                        callBack.onRetrievalFailed("resent api error !")
                    }
                } else {
                    executors.mainThread().execute {
                        callBack.onMessageRetrieved(flickrResent)
                    }
                }
            }
        }
    }
}