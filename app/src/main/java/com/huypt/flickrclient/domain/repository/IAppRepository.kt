package com.huypt.flickrclient.domain.repository

import com.huypt.flickrclient.domain.model.FlickrResent


interface IAppRepository {
    fun getListPhoto(): FlickrResent?

}