package com.huypt.flickrclient.domain.model

import com.huypt.flickrclient.presentation.model.PhotosModel


data class Photos(
        val perpage: Int = 0,
        val total: Int = 0,
        val pages: Int = 0,
        val photoItem: List<PhotoItem>,
        val page: Int = 0) {

    fun toPhotosModel() = PhotosModel(perpage,
            total,
            pages,
            photoItem.map { it -> it.toPhotoItemModel() },
            page)
}