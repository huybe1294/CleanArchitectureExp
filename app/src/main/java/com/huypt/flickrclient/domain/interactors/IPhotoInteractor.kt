package com.huypt.flickrclient.domain.interactors

import com.huypt.flickrclient.domain.model.FlickrResent


interface IPhotoInteractor:BaseInteractor {

    interface CallBack {
        fun onMessageRetrieved(flickrResent: FlickrResent)
        fun onRetrievalFailed(error: String)
    }
}