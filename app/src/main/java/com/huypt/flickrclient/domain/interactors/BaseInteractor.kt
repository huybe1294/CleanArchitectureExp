package com.huypt.flickrclient.domain.interactors

interface BaseInteractor {
    fun execute()
}