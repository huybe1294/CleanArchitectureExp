package com.huypt.flickrclient.domain.executor

import java.util.concurrent.Executor

interface IExecutor {

    fun diskIO(): Executor

    fun networkIO(): Executor

    fun mainThread(): Executor
}