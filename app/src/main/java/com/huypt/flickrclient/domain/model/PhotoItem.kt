package com.huypt.flickrclient.domain.model

import com.huypt.flickrclient.presentation.model.PhotoItemModel

data class PhotoItem(
        val owner: String = "",
        val server: String = "",
        val heightS: String = "",
        val widthS: String = "",
        val urlS: String = "",
        val ispublic: Int = 0,
        val isfriend: Int = 0,
        val farm: Int = 0,
        val id: String = "",
        val secret: String = "",
        val title: String = "",
        val isfamily: Int = 0) {

    fun toPhotoItemModel() = PhotoItemModel(owner, server, heightS, widthS, urlS, ispublic, isfriend,
            farm, id, secret, title, isfamily)
}