package com.huypt.flickrclient.domain.model

import com.huypt.flickrclient.presentation.model.FlickrResentModel

data class FlickrResent(
        val stat: String = "",
        val photos: Photos) {
    fun toFlickrResentModel() = FlickrResentModel(stat, photos.toPhotosModel())
}